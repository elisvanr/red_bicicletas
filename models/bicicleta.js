var Bicicleta = function (id,color,modelo,ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}
Bicicleta.prototype.toString= function(){
    return 'id: '+this.id+'| color: '+this.color ;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}


Bicicleta.findById=function(aBiciId){
    var aBici=Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici) {
        return aBici;
    }else{
        throw new Error(`No existe una bicicleta con esa id ${aBiciId}`);
    }
}

Bicicleta.removeById = function(aBiciId){
    for (let i = 0; i < Bicicleta.allBicis.length; i++) {
        if (Bicicleta.allBicis[i].id == aBiciId) {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
        
    }

}


var a = new Bicicleta(1,'rojo','urbana',[10.982236,-74.781576]);
var b = new Bicicleta(2,'azul','urbana',[10.9821727,-74.7836507]);

Bicicleta.add(a);
Bicicleta.add(b);
module.exports= Bicicleta;